using MangaWeb.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

var builder = WebApplication.CreateBuilder(args);

// Ajoutez des services � l'injection de d�pendances ici.
builder.Services.AddControllersWithViews();

// Configurez HttpClient pour les services Manga et User
builder.Services.AddHttpClient<MangaService>(client =>
{
    client.BaseAddress = new Uri(builder.Configuration["ApiBaseUri:Mangas"]);
});
builder.Services.AddHttpClient<UserService>(client =>
{
    client.BaseAddress = new Uri(builder.Configuration["ApiBaseUri:Users"]);
});

var app = builder.Build();

// Configurez la pipeline de requ�te HTTP ici.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseExceptionHandler("/Home/Error");
    // La valeur par d�faut HSTS est de 30 jours. Vous pouvez changer cela pour des sc�narios de production.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
