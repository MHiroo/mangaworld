﻿using Newtonsoft.Json;
using Mangapedia.Models;
using System.Text;


namespace MangaWeb.Services
{
    public class MangaService
    {
        private readonly HttpClient _httpClient;
        private readonly string _mangasApiUri;
        private readonly string _authorsApiUri;

        public MangaService(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient;
            _mangasApiUri = configuration["ApiBaseUri:Mangas"];
            _authorsApiUri = configuration["ApiBaseUri:Authors"];
        }

        public async Task<IEnumerable<Author>> GetAuthorsAsync()
        {
            var response = await _httpClient.GetAsync(_authorsApiUri);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var authors = JsonConvert.DeserializeObject<IEnumerable<Author>>(content);
                return authors;
            }
            return new List<Author>();
        }

        public async Task<bool> AddMangaAsync(Manga manga)
        {
            var json = JsonConvert.SerializeObject(manga);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(_mangasApiUri, content);
            return response.IsSuccessStatusCode;
        }
        public async Task<IEnumerable<Manga>> GetAllMangasAsync()
        {
            var response = await _httpClient.GetAsync(_mangasApiUri);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<Manga>>(content);
            }
            else
            {
                // Handle the error, log it, or throw an exception as per your error handling policy
                return new List<Manga>();
            }
        }
        public async Task<Manga> GetMangaByIdAsync(int id)
        {
            var response = await _httpClient.GetAsync($"{_mangasApiUri}/{id}");
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Manga>(content);
            }
            else
            {
                // Handle the error, log it, or throw an exception as per your error handling policy
                return null;
            }
        }
        public async Task<bool> DeleteMangaAsync(Manga manga)
        {
            var response = await _httpClient.DeleteAsync($"{_mangasApiUri}/{manga.MangaId}");
            return response.IsSuccessStatusCode;
        }


    }
}