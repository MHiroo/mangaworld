﻿using Microsoft.AspNetCore.Mvc;
using MangaWeb.Services;
using Mangapedia.Models;
using System.Threading.Tasks;
using System.Linq;

namespace MangaWeb.Controllers
{
    public class MangaController : Controller
    {
        private readonly MangaService _mangaService;

        public MangaController(MangaService mangaService)
        {
            _mangaService = mangaService;
        }
        public async Task<IActionResult> DeleteManga()
        {
            var mangas = await _mangaService.GetAllMangasAsync();
            return View(mangas);
        }

        // GET: Affiche la vue pour ajouter un nouveau Manga
        [HttpGet]
        public async Task<IActionResult> AddManga()
        {
            var authors = await _mangaService.GetAuthorsAsync();
            // Assurez-vous que les auteurs sont bien passés à la vue, même si la liste est vide
            ViewBag.Authors = authors.ToList();
            return View();
        }

        // POST: Ajoute un Manga à l'API
        [HttpPost]
        public async Task<IActionResult> AddManga(Manga manga)
        {
            if (ModelState.IsValid)
            {
                bool result = await _mangaService.AddMangaAsync(manga);
                if (result)
                {
                    // Redirige vers l'action "Accueil" du contrôleur "Home"
                    return RedirectToAction("Accueil", "Home");
                }
                else
                {
                    // Traite l'échec (par exemple en affichant un message d'erreur)
                    ModelState.AddModelError(string.Empty, "Une erreur s'est produite lors de l'ajout du manga.");
                }
            }

            // Si le modèle n'est pas valide, ou si l'ajout échoue, recharge la liste des auteurs
            var authors = await _mangaService.GetAuthorsAsync();
            ViewBag.Authors = authors.ToList();
            // Renvoie à la vue avec les informations déjà saisies par l'utilisateur
            return View(manga);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteManga(int mangaId)
        {
            // Récupérer le manga à supprimer en fonction de son ID
            var mangaToDelete = await _mangaService.GetMangaByIdAsync(mangaId);

            // Vérifier si le manga existe
            if (mangaToDelete == null)
            {
                // Gérer le cas où le manga n'existe pas
                return NotFound();
            }

            // Appeler le service pour supprimer le manga
            bool deletionResult = await _mangaService.DeleteMangaAsync(mangaToDelete);

            // Vérifier le résultat de la suppression
            if (deletionResult)
            {
                // Rediriger vers une page de confirmation ou une autre action
                return RedirectToAction("DeleteManga", "Manga"); 
            }
            else
            {
                // Gérer le cas où la suppression a échoué
                ModelState.AddModelError(string.Empty, "Une erreur s'est produite lors de la suppression du manga.");
                // Recharger la vue avec le message d'erreur
                return View("DeleteManga");
            }
        }

    }
}