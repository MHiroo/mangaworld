﻿using Microsoft.AspNetCore.Mvc;
using MangaWeb.Services;
using MangaWeb.Models;

namespace MangaWeb.Controllers
{
    public class UserController : Controller
    {
        private readonly UserService _userService;

        public UserController(UserService userService)
        {
            _userService = userService;
        }

        // Afficher le formulaire
        public IActionResult Create()
        {
            return View();
        }

        // Traiter le formulaire
        [HttpPost]
        public async Task<IActionResult> Create(User user)
        {
            if (ModelState.IsValid)
            {
                await _userService.CreateUserAsync(user);
                return RedirectToAction("Index", "Home"); // Redirigez selon vos besoins
            }
            return View(user);
        }
    }
}
