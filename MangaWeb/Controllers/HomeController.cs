﻿using MangaWeb.Models;
using MangaWeb.Services;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Threading.Tasks;

namespace MangaWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly MangaService _mangaService;

        public HomeController(ILogger<HomeController> logger, MangaService mangaService)
        {
            _logger = logger;
            _mangaService = mangaService;
        }

        public async Task<IActionResult> Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public async Task<IActionResult> accueil()
        {
            var mangas = await _mangaService.GetAllMangasAsync(); // Implémentez cette méthode dans votre service
            return View(mangas);
        }
        public async Task<IActionResult> Details(int id)
        {
            var manga = await _mangaService.GetMangaByIdAsync(id); // Implémentez cette méthode pour récupérer un manga par son ID
            if (manga == null)
            {
                return NotFound();
            }
            return View(manga);
        }
    }
}
